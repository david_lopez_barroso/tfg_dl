import geopy as geo
import numpy as np
import pandas as pd


NumeroPuntos = 300
CotaSupLat = -18.2441
CotaInfLat = -46.72
CotaSupLong = -75.3435
CotaInfLong = -67.3154
Pais = ['Chile']
NombreExcel = 'data/hola'


'''
Genera coordenadas de un pais

param NumeroPuntos (numero): numero de coordenadas a crear
param CotaSupLat (numero): cota superior de latitud
param CotaInfLat (numero): cota inferior de latitud
param CotaSupLong (numero): cota superior de latitud
param CotaInfLong (numero): cota inferior de latitud
param Pais (lista): pais o paises a los cuales deben pertenecer las coordenadas
param csv (NombreExcel): if False no exporta, si no representa el nombre del archivo
return: pandas dataframe con columnas lat | lon

require
geopy as geo: comprobar localizacion de las coordenadas
numpy as np: generacion de coordenadas aleatorias
pandas as pd: devuelve un dataframe

'''

CoordenadasSalida = []
CountCoordenadas = 0

while CountCoordenadas != NumeroPuntos:
    LatPropuesta = round(np.random.uniform(CotaSupLat, CotaInfLat), 4)
    LonPropuesta = round(np.random.uniform(CotaSupLong, CotaInfLong), 4)
    geolocator = geo.Nominatim(timeout=10)
    location = geolocator.reverse('"' + str(LatPropuesta) + ',' + str(LonPropuesta) + '"')
    if location.address != None:
        Direccion = location.address

        k = Direccion.count(',')
        if k > 1:
            position = []
            for p in range(0, k):
                position.append([j for j, x in enumerate(Direccion) if x == ","][p] + 1)

            Info = [None] * (len(position))
            j = 0
            Info[j] = Direccion[(position[j] + 1):(position[j + 1] - 1)]
            for j in range(1, len(position) - 1):
                Info[j] = Direccion[(position[j] + 1):(position[j + 1]) - 1]

            j = len(position) - 1
            Info[j] = Direccion[(position[-1] + 1):(len(Direccion))]
            if Info[j] in Pais:
                CoordenadasSalida.append([LatPropuesta, LonPropuesta])
        elif k == 1:
            position = []
            position.append([j for j, x in enumerate(Direccion) if x == ","][0] + 1)

            Info = [None] * (len(position))
            Info[0] = Direccion[(position[-1] + 1):(len(Direccion))]
            if Info in Pais:
                CoordenadasSalida.append([LatPropuesta, LonPropuesta])

        elif k == 0:
            if Direccion in Pais:
                CoordenadasSalida.append([LatPropuesta, LonPropuesta])

        CountCoordenadas += 1

CoordenadasSalida = pd.DataFrame(CoordenadasSalida)
CoordenadasSalida.columns = ['lat', 'lon']
if NombreExcel != False:
    writer = pd.ExcelWriter(NombreExcel + '.xlsx', engine='xlsxwriter')
    CoordenadasSalida.to_excel(writer, sheet_name='Coordenadas', index=False)
    writer.save()




