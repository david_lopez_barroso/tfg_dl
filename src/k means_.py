import pandas as pd

import Functions_ as fun

# Input parameters
meth = 'kmeans'
n_cluster_inicial = 3
num_clusters_final = 3
# Parametros por defecto
max_demand = 50000
n_cluster_limite = num_clusters_final + 1

# Read files
filename_1 = 'data/Chi_sur.xlsx'
filename_2 = 'data/Esp_demandas_neutro.xlsx'
df_contratas = pd.read_excel(filename_1)
df_demandas = pd.read_excel(filename_2)

if filename_1 == 'data/Esp_original.xlsx' or filename_1 == 'data/Esp_bal_can.xlsx' or filename_1 == 'data/Esp_bal.xlsx' or filename_1 == 'data/Esp_can.xlsx':
    data_name = 'España con outlier'
    duplicate = True
elif filename_1 == 'data/Esp_peninsular.xlsx':
    data_name = 'España sin outlier'
    duplicate = True
elif filename_1 == 'data/Chi_original.xlsx' or filename_1 == 'data/Chi_sur.xlsx':
    data_name = 'Chile con outlier'
    duplicate = False
elif filename_1 == 'data/Chi_bien.xlsx':
    data_name = 'Chile sin outlier'
    duplicate = False

total_within = []
total_between = []
total_ratio = []

# Prepare data
if duplicate == True:
    df_data_complete = fun.prepare_data(df_demandas, df_contratas, max_demand=max_demand, duplicate=duplicate)
elif duplicate == False:
    df_data_complete = df_contratas[["longitud", "latitud"]]

for k in range(n_cluster_inicial, n_cluster_limite):

    print(' ')
    print('Número de clusters: ' + str(k))

    # Number of clusters
    n_clusters = k

    # Create clusters
    l_result = fun.create_clusters(df_data_complete[["longitud", "latitud"]], n_clusters=n_clusters, func=meth)

    # Adapt data when duplicates
    if duplicate == True:
        result_length = len(df_data_complete.groupby(by=["codigo_contrata", "codigo_material"]))
        df_data_result = df_data_complete.iloc[range(0, result_length)]
        long = df_data_result["longitud"]
        lat = df_data_result["latitud"]
        a_clusters = l_result[range(0, result_length)]

    # Create and show map
    centers = fun.show_map(l_result, df_data_complete, data_name, meth, duplicate)
    print('Centros')
    print(centers)
    # Calculate total within sum of squares
    W1 = fun.calcWithinGroupsVariance(df_data_complete["longitud"], l_result)
    W2 = fun.calcWithinGroupsVariance(df_data_complete["latitud"], l_result)
    total_within.append(W1+W2)
    # Calculate total between sum of squares
    B1 = fun.calcBetweenGroupsVariance(df_data_complete["longitud"], l_result)
    B2 = fun.calcBetweenGroupsVariance(df_data_complete["latitud"], l_result)
    total_between.append(B1+B2)
    # Calculate ratio
    total_ratio.append((B1+B2)/(W1+W2+B1+B2))

print('Total within sum of squares')
print(total_within)
print('Total between sum of squares')
print(total_between)
print('Ratio')
print(total_ratio)

# Show Elbow code
if (num_clusters_final - n_cluster_inicial) > 0:
    fun.show_elbowcode(total_within, n_cluster_inicial, num_clusters_final, data_name, meth)

# # Show ratio
# if (num_clusters_final - n_cluster_inicial) > 0:
#     fun.show_ratio(total_ratio, n_cluster_inicial, num_clusters_final, data_name, meth)