\chapter{Estado del arte. Agrupamiento}\label{Capitulo2}
Este capítulo explica todos los conceptos necesarios para el estudio desarrollado en 
este TFG.  
Se comienza con una introducción de qué es el agrupamiento. Después, se definen los 
algoritmos seleccionados y el criterio para seleccionar el número de grupos adecuado. 
Finalmente, se proporcionan herramientas para la comparación entre los distintos algoritmos.

	\section{Introducción al agrupamiento}
	El ``aprendizaje automático'' o ``machine learning'' es una rama de la inteligencia artificial que
	consiste en el reconocimiento de patrones en un conjunto de datos (\cite{12}). Dentro de este campo se 
	encuentra el método de ``aprendizaje no supervisado''. Este método trata los objetos de entrada 
	como un conjunto de variables aleatorias y su función
	es la agrupación de las mismas mediante técnicas de agrupamiento. La finalidad de estas técnicas es, 
	mediante un algoritmo que procese estas observaciones, encontrar una estructura 
	en las mismas que las agrupe según su similitud.
	
	Estas técnicas son aplicadas en diversos ámbitos como la 
	biología (clasificación de especies), medicina (investigación de 
	enfermedades), marketing (identificación de segmentos de mercado) o 
	biometría (identificación facial). La aplicación que ocupa este TFG es 
	la agrupación de datos geográficos.
	 
	Los algoritmos de agrupamiento consisten en agrupar una muestra de datos según un criterio. 
	Este criterio se rige por la similitud, es decir, se agrupan datos similares. 
	La similitud entre dos elementos de la muestra se mide mediante una métrica. 
	Los algoritmos de agrupamiento permiten introducir distintas métricas, entre las que 
	destacan: la euclídea, la de Minwoski o la de Canberra. 
	
	Este estudio agrupará puntos 
	geográficos, por tanto, se escoge la distancia euclídea como aproximación a la distancia 
	sobre el elipsoide de referencia.
	En un espacio bidimensional, la distancia euclídea entre los puntos 
	$ X = (x_1, x_2) $ e $ Y=(y_1, y_2) $, se denota $ d_E(X,Y) $ y se define como sigue:
	\begin{equation}
	\begin{split}
	d_E(X, Y) = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}&\\
	\end{split}
	\end{equation}
	
	El resultado del algoritmo son grupos cuyos elementos comparten propiedades comunes, por lo que los grupos se 
	caracterizan por la homogeneidad dentro de cada uno y la heterogeneidad entre ellos.
	
	Existen tres bloques principales de técnicas de agrupamiento:
		\begin{itemize}
		\item	Agrupamiento jerárquico: 
			\begin{itemize}
			\item	Aglomerativos o ascendentes: parten de una muestra sin agrupar. De forma iterativa, 
			asocian elementos de la muestra. De este modo, crean grupos o añaden elementos a grupos 
			existentes. En la última iteración todos 
			los individuos pasan a forman parte del mismo conglomerado. 
			\item	Disociativos o descendentes: parten de una muestra agrupada en un mismo conglomerado. De forma
			iterativa, separan un grupo existente para crear otros dos. En la última iteración existen tantos grupos
			como individuos.
			\end{itemize}
		\item	Agrupamiento no jerárquico:  
			\begin{itemize}
			\item	Particionales: parten de una agrupacion inicial. De forma iterativa, asignan nuevos grupos a cada individuo 
			de la muestra según un criterio. La última iteración se produce cuando el algoritmo se estabiliza
			y no se producen más reasignaciones.
		\end{itemize}
		\item 	Agrupamiento basado en técnicas estadísticas: algoritmos que seleccionan un modelo matemático o estadístico para los datos.
		Buscan valores para los parámetros que mejor representan el comportamiento de la muestra.
		\end{itemize}
	
	\section{Métodos jerárquicos}\label{agrupamiento}
	Como se ha explicado anteriormente, existen dos tipos de métodos 
	jerárquicos: aglomerativos y disociativos.  Este TFG recoge los cuatro algoritmos aglomerativos
	más extendidos: enlace simple, enlace promedio, enlace completo y método de
	Ward. Estos métodos se diferencian por el criterio según el cual se produce el agrupamiento (\cite{10}). 
	
	En los métodos jerárquicos aglomerativos, inicialmente hay tantos grupos como individuos. La mayoría de individuos
	similares se agrupan primero y se combinan de acuerdo a sus similitudes. Finalmente, a medida que la similitud
	disminuye, todos los grupos se fusionan en un solo grupo.
	
	Dentro de los métodos jerárquicos aglomerativos existen tres métodos de tipo enlace: simple, promedio y completo. Cada uno de ellos realiza las agrupaciones
	bajo un criterio particular. No obstante, todos siguen el mismo proceso de agrupamiento. A continuación, se detalla dicho proceso para agrupar una muestra de $N$ 
	individuos:

	\begin{enumerate}
		\item	Empieza con $ N $ grupos compuestos por un individuo cada uno y se crea una matriz simétrica $\textbf{D}=(d_{ij})$, $\textbf{D} \in \mathbb{M} (\mathbb{R})_{N\times N }$
		llamada matriz de distancias $ \textbf{D} = {d_{ij}} $.
		
		De esta manera, el elemento $ d_{ij} $ corresponde a la distancia entre los grupos $ i $ y $ j $.
		Cualquier distancia debe cumplir la propiedad de simetría, es decir, 
		$d(x,y) = d(y,x)$. Por tanto, $\textbf{D}$ es una matriz simétrica de dimensión $N \times N$, donde $N$ es el 
		número de grupos. Además, cualquier distancia debe cumplir la siguiente propiedad: $d(x,y) = 0 \iff x=y$.
		Esta propiedad implica que los elementos de la diagonal de $\textbf{D}$ son nulos, es decir $d_{ii}= 0$, $\forall 
		i=0, \dots, N$.
		\item	Busca, para cada par de grupos, los individuos más similares entre si. Cada uno de estos miembros pertenece a un grupo. 
		Esta similitud se mide por una distancia que dependerá del método de enlace. Sean los grupos $ U $ y $ V $ los más cercanos, y su distancia $ d_{UV} $.
		\item Enlaza los grupos $ U $ y $ V $. Se renombra el grupo recién formado como $ (UV) $. Actualiza las entradas
		en la matriz de distancias, eliminando las filas y columnas correspondientes
		a los grupos U y V. Introduce una fila y una columna que representan las distancias del nuevo grupo, 
		$ (UV) $, con el resto de los grupos. Según el tipo de enlace, esta distancia
		se establece en función del criterio que lo gobierna. 
		\item Repite los pasos 2 y 3 un total de $ N - 1 $ veces. Todos los individuos pertenecen a un solo
		grupo después de que tras $N-1$ iteraciones el algoritmo finalice. Guarda la identidad de los grupos que
		se enlazan y los niveles. Dichos niveles representan la distancia a la que se dan lugar las agrupaciones. 
	\end{enumerate}
	
		\subsection{Enlace simple}		
		La distancia entre dos grupos se calcula como la mínima distancia
		para cada par de individuos. Cada uno de estos deberá pertenecer a un grupo distinto. 
		Considérense los grupos U, V y W, los cuales contienen al menos un individuo. Supóngase que U y V han sido unidos 
		en una iteración anterior. La distancia del método del enlace simple se denota como $ d_{(UV)W} $ y queda definida por 
		la ecuación \ref{2.2}. La figura \ref{simple} ilustra dicha distancia.
		
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=10cm]{img/simple}
			\caption{Enlace simple}
			\label{simple}
		\end{figure}
		
		\begin{equation}\label{2.2}
			\begin{split}
			d_{(UV)W} = \min(d_{UW}, d_{VW})&\\
			\end{split}
		\end{equation}

		\subsection{Enlace completo}
		La distancia entre dos grupos se calcula como la máxima distancia
		para cada par de individuos. Cada uno de estos deberá pertenecer a un grupo distinto. 
		Considérense los grupos U, V y W, los cuales contienen al menos un individuo. Supóngase que U y V han sido unidos 
		en una iteración anterior. La distancia del método del enlace completo se denota como $ d_{(UV)W} $ y queda definida por 
		la ecuación \ref{2.3}. La figura \ref{completo} ilustra dicha distancia.

		\begin{figure}[H]
			\centering
			\includegraphics[width=10cm]{img/completo}
			\caption{Enlace completo}
			\label{completo}
		\end{figure}

		\begin{equation}\label{2.3}
		\begin{split}
		d_{(UV)W} = \max(d_{UW}, d_{VW})&\\
		\end{split}
		\end{equation}
	
		\subsection{Enlace promedio}
		La distancia entre dos grupos se calcula como la distancia media
		entre todos los pares de individuos. Cada uno de estos deberá pertenecer a un grupo distinto. 
		Considérense los grupos U, V y W, los cuales contienen al menos un individuo. Supóngase que U y V han sido unidos 
		en una iteración anterior. La distancia del método del enlace promedio se denota como $ d_{(UV)W} $ y queda definida por 
		la ecuación \ref{2.4}. La figura \ref{promedio} ilustra dicha distancia.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=10cm]{img/promedio}
			\caption{Enlace promedio}
			\label{promedio}
		\end{figure}
		
		\begin{equation}\label{2.4}
		d_{(UV)W} = \frac{\sum_{i}\sum_{k}{d_{ik}}}{N_{(UV)}N_W}
		\end{equation}
		
		\subsection{Método de Ward}
		El método de Ward se diferencia de los anteriores en cuanto al criterio
		y a la manera de proceder. Ward, consideró métodos jerárquicos basados en 
		``minimzar la pérdida de información'' al unir dos grupos. El algoritmo definido por Ward busca el menor 
		incremento de EES, de sus siglas en inglés ``sum of squared errors of prediction'', en cada grupo.
		
		Según el criterio de la ``mínima pérdida de información'', las agrupaciones se realizan 
		uniendo grupos a pares, como en los métodos anteriores, de forma que 
		el aumento de EES sea mínimo.
		
		Primero, para un grupo $ k $ dado, sea $ ESS_k $ la suma de las desviaciones cuadradas de
		cada elemento en el grupo a su centroide. Si actualmente hay $ K $
		grupos, se define $ ESS $ como la suma del $ EES$ de cada grupo, es decir, $ ESS = ESS_1 + ESS_2 + ... + ESS_k  $. 
		En cada paso del análisis, se considera la unión de cada par de grupos.
		Se unen los dos grupos con en el menor aumento de ESS (mínima
		pérdida de información). 
		
		De este modo, en la primera iteración de este algoritmo ningún individuo está agrupado, por tanto, 
		$EES = 0$. Cuando todos los individuos pertenecen al mismo grupo, el $EES$ queda definido por la ecuación
		\ref{2.5}.
			\begin{equation}\label{2.5}
			\begin{split}
			EES = \sum_{j=1}^{N}(\textbf{x}_{j}-\bar{\textbf{x}})(\textbf{x}_{j}-\bar{\textbf{x}})^T&\\
			\end{split}
			\end{equation}
		donde $ \bar{\textbf{x}} $ es la media de los individuos 
		(centroide) y \textbf{x}\textsubscript{j} denota el individuo $ j $.
		
		\subsection{Otros métodos. BIRCH}
		Los algoritmos jerárquicos anteriormente descritos no suelen ser eficaces al
		analizar grandes conjuntos de registros. Esto se debe al tratamiento y análisis de los
		datos que realizan. El método BIRCH, siglas en inglés de ``balanced iterative reducing
		and clustering using hierarchies'', es un algoritmo de agrupamiento jerárquico diseñado
		para agrupar grandes muestras de datos. Este algoritmo constituye uno de los primeros
		acercamientos de la estadística al entorno del Big Data (\cite{16}).
		
		El algoritmo BIRCH integra la estrategia del agrupamiento jerárquico con otros
		métodos de agrupamiento. Esta combinación permite que el algoritmo pueda utilizarse
		ante muestras grandes y permite hacer reasignaciones a lo largo del proceso iterativo.
		
		Se puede decir que BIRCH es un algoritmo de agrupamiento basado en un
		árbol de características y en las subcaracterísticas de dicho árbol. Para definir dichas
		características se definen los conceptos siguientes:
		
		\begin{itemize}
			\item	Característica del agrupamiento o CF, siglas en inglés de ``clustering feature'', es
			un vector tridimensional que resume la información sobre los individuos de un
			grupo. Se define como $ CF =< n;LS;SS > $, donde $ n $ es el número de individuos
			del grupo, $ LS $ es la suma de los $ n $ puntos y $ SS $ es la suma cuadrada de los $ n $
			puntos.
			\item	Árbol de características, o CF tree, es un árbol balanceado que almacena los CFs
			para una agrupación jerárquica.
		\end{itemize}
	
		De este modo, un CF resume las características de un grupo. Estas características
		cumplen la propiedad aditiva. Por tanto, dados dos grupos separado, C1 y C2, cuyas
		características son CF1 y CF2, y un nuevo grupo creado por la unión de C1 y C2, se
		puede calcular la característica de este nuevo grupo como las sumas de los grupos que
		lo componen, es decir, por CF1+CF2.
		
		Cabe señalar que los CFs son suficientes para calcular todas las medidas que
		son necesarias para la toma de decisiones realizadas en el algoritmo BIRCH. Por tanto,
		no es necesario trabajar con la matriz de datos original, basta con utilizar los CFs. Este
		hecho es el que agiliza el proceso iterativo y el que hace que BIRCH sea un algoritmo
		adecuado para muestras grandes.
		
		Por otra parte, el árbol de características se compone por dos parámetros: factor
		de bifurcación, B, y el umbral, T. Mediante estos parámetros se establecen las posibles
		relaciones entre las ramas y las hojas del árbol a modo de cadena. El algoritmo agrupa
		hojas del árbol de manera que, las entradas en las hojas no se forman por un único
		individuo, sino un por subgrupo.
		
		El algoritmo BIRCH intenta obtener los mejores grupos con los recursos
		disponibles. Para ello, el algoritmo aplica una técnica multifase con una única lectura
		a los datos de entrada. Puntualmente, se puede realizar una segunda lectura para
		perfeccionar la calidad del resultado. El algoritmo se estructura en dos pasos principales:
		
		\begin{enumerate}
			\item	Lectura de datos de entrada. Construcción de un árbol de características inicial.
			Este árbol puede ser visto como una composición multinivel de los datos que
			intenta mantener las características de la estructura grupal de los datos.
			\item	Agrupación del árbol creado haciendo uso de algún algoritmo de agrupamiento.
		\end{enumerate}
		
		Este algoritmo también se conoce como el algoritmo de agrupación en dos pasos,
		debido a los dos pasos descritos.
		
		Mediante la implementación de este algoritmo se esperara obtener soluciones en
		un menor tiempo computacional que al aplicar los algoritmos anteriormente descritos.
		Además, se compararán con detalle la calidad del agrupamiento obtenido.
		
	\section{Métodos no jerárquicos}
	El método más extendido por su versatilidad es el de K-medias, también
	conocido por su nombre en inglés, ``K-means''. Este algoritmo es de tipo particional. 
	Cabe destacar que este método necesita un número de grupos fijado de antemano para 
	proceder al agrupamiento.
	
		\subsection{K-medias}\label{Kmedias}
		K-medias es un proceso iterativo mediante reasignación, el cual 
		usa el estado anterior para mejorar el siguiente. Este algoritmo
		trabaja con la matriz de datos original, en lugar de con la de 
		distancias anteriormente descrita. Por lo tanto, requiere menor memoria que los métodos 
		anteriores. Lo que hace que este método proporcione mejores resultados computacionales 
		al agrupar una muestra grande que los métodos anteriormente descritos (\cite{11}).
		
		El algoritmo consta de cuatro pasos:
		\begin{enumerate}
		\item	Inicialización: se parte de una división inicial de los 
		datos en un número $ k $ de grupos. Esta división puede ser aleatoria 
		o seguir algún criterio. Este puede ser la suma de sus variables o la solución 
		de otro método, jerárquicos normalmente.
		Con este agrupamiento inicial se obtienen los $ k $ centroides, que pueden ser calculados como la media de los individuos de cada grupo.
		\item	Asignación de individuos a centroides: cada individuo es 
		asignado al centroide más cercano.
		\item	Actualización de los centroides: se actualiza la posición 
		del centroide de cada grupo una vez realizadas las nuevas asignaciones.
		\item	Repetir los pasos 2 y 3 hasta que no se producen reasignaciones 
		o se alcanza un número máximo de iteraciones.
		\end{enumerate}
	
		Al terminar el paso cuatro los grupos se habrán estabilizado.
		
		K-medias trata de minimizar la suma de las distancias 
		cuadráticas de los individuos al centroide del grupo al que 
		pertenecen. Los nuevos centroides se toman como la media de los individuos de cada grupo.
		
		La función a minimizar es la siguiente:
					
			\begin{equation}
			\begin{split}
			\min{(\sum_{k}\sum_{j \in k} ||x_{jk} - c_{k} ||^2)}
			\end{split}
			\end{equation}
		
		donde $ k $ es el número de grupo, $x_{kj}$ es cada uno de los individuos que pertenecen
		al grupo $k$ y $c_k$ es el centroide de dicho grupo.
		
	\section{Basado en técnicas estadísticas}	
		\subsection{EM}
		El algoritmo EM (``Expectation-maximization'') fue descrito y presentado por Dempster et al. (1977). Este
		algoritmo es usado en 
		estadística para encontrar estimadores de máxima verosimilitud de una 
		serie de parámetros en modelos probabilísticos que dependen de variables 
		no observables. Posee ciertas ventajas atractivas comparado con otros algoritmos iterativos. 
		Por ejemplo, la economía de almacenamiento, la facilidad de	implementación y la estabilidad 
		numérica.
		
		Este tipo de método trata de obtener la FDP (función de 
		densidad de probabilidad) desconocida a la que pertenece el conjunto de 
		datos, la cual se puede aproximar mediante una combinación lineal 
		definida por esos parámetros. Así, trata solucionar 
		el problema del desconocimiento de qué individuos provienen de qué 
		componente, obteniendo así un conjunto de grupos compuestos por el
		conjunto de datos.
		
		La implementación de este algoritmo se realiza mediante un modelo de 
		mezcla gaussiana, un modelo probabilístico que asume que todos los 
		individuos son generados a partir de una mezcla de un número finito 
		de distribuciones gaussianas con parámetros desconocidos.
		
		El algoritmo EM empieza estimando los parámetros de las distribuciones 
		y los usa para calcular las probabilidades de que cada individuo pertenezca 
		a un grupo y usa esas probabilidades para re-estimar los parámetros de las probabilidades, 
		hasta converger.
		
		Para ello, opera de forma iterativa siguiendo dos pasos: 
		\begin{itemize}
			\item Paso E: utiliza los valores de los parámetros iniciales o proporcionados
			por el paso M de la iteración anterior, obtiene diferentes formas de la FDP
			buscada.
			\item Paso M: se computan estimadores de máxima verosimilitud de los 
			parámetros mediante la maximización de la verosimilitud esperada, 
			estos parámetros se usan para repetir el paso E.
		\end{itemize}
		
		En resumen, se calcula para cada individuo la probabilidad 
		de que sea generado por cada componente y, a continuación, se 
		modifican los parámetros para maximizar la probabilidad de los datos 
		a esas asignaciones.
		
		Aunque EM garantiza convergencia, esta puede ser a un máximo local, por lo que se recomienda repetir el proceso varias veces.
		
 

	\section{Ventajas y desventajas}
	Una vez conocidos los grandes bloques de agrupamiento y las diferentes
	técnicas, se pueden detallar sus ventajas y desventajas. 
	
	Por un lado, los métodos jerárquicos no necesitan un número de grupos 
	conocido previamente. En cambio, los métodos no jerárquicos sí lo necesitan para poder 
	proceder con las iteraciones. De este modo, parten de unos grupos iniciales con un centro 
	identificado.
	
	Por lo general, los métodos jerárquicos son sensibles a valores atípicos 
	o a ``ruido'' en la muestra. Un individuo que puede haber sido asignado de manera errónea 
	en etapas tempranas puede no ser corregido o no ser reasignado al grupo adecuado. 
	
	Por otra parte, como los métodos no jerárquicos realizan una asignación inicial, pueden 
	llevar a agrupaciones no deseadas si la agrupación inicial no es adecuada. Para evitar este 
	problema, se puede inicializar el algoritmo varias veces 
	partiendo de asignaciones aleatorias distintas en cada ejecución.
	Algunos de los efectos pueden ser: ignorar diferencias entre datos claramente 
	diferentes, pero inicialmente juntos. También pueden generar un grupo aislado que 
	solo contenga un valor atípico, generar un grupo producido por una pequeña 
	cantidad de valores atípicos o incluso ignorar este grupo de valores atípicos 
	cuando el número de grupos es conocido y adecuado.
	
	Por todo ello, es necesario analizar con cuidado las soluciones obtenidas 
	e introducir perturbaciones mediante nuevos valores para verificar el 
	comportamiento de la agrupación antes y después de la perturbación introducida. 

	Una vez expuestos los algoritmos seleccionados en este TFG, se
	ilustra su comportamiento. Para ello, la figura \ref{graficos_python} muestra 
	los distintos grupos que genera cada algoritmo partiendo de la misma  muestra.
	
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=11cm]{img/graficos_teoricos}
		\caption{Ejemplos de agrupaciones}
		\label{graficos_python}
	\end{figure}
	
	\section{Elección del número de grupos}
	En el ámbito de los métodos de agrupamiento, se debe seleccionar el número de grupos
	con el que caracterizar la solución.
	 
	Los métodos no jerárquicos necesitan este dato previamente. Lo más adecuado es obtener 
	soluciones para un rango de valores y comparar los resultados. Aunque 
	los jerárquicos pueden realizar el análisis sin él, puede ser necesario 
	determinar el número de grupos para determinar una solución.
		
	Una de las medidas que más se utilizan es la distancia media entre los 
	individuos de cada grupo y el centroide del grupo al que pertenecen. De este modo, 
	cuando se aumenta el número de grupos, la medida disminuye. Si el número de grupos
	es igual al número de individuos, la medida será nula.
	
	Para realizar esté análisis se hará uso del método de Elbow. Además, en
	el caso de los métodos jerárquicos, esta elección también puede tomarse usando un dendrograma.
	Ambos métodos se detallan a continuación.
	
	Antes de nada, se deben definir dos conceptos matemáticos: SSW y SSB.
	
	\begin{itemize}
		\item Suma de cuadrados dentro de cada grupo o SSW, siglas en inglés de ``Within Sum of Squares``:
		\begin{equation}\label{eqn:within}
		SSW = \sum_v\sum_{k}\sum_{j\in{k}}(x_{vkj} - \bar{x}_{vk})^{2}
		\end{equation}
		siendo $v$ cada una de las variables independientes, $ k $ el número de grupo, $ j $ los datos 
		correspondientes al grupo $k$ y $\bar{x}_{vk}$ la media de los datos que corresponden a ese grupo.
		\item Suma de cuadrados entre grupos o SSB, siglas en inglés de ``Between Sum of Squares``:
		\begin{equation}\label{eqn:between}
		SSB = \sum_{v}\sum_{k} n_k (\bar{x_{vk}} - \bar{x}_{v})^{2}
		\end{equation}
		siendo $v$ cada una de las variables independientes, $ k $ el número de grupo, $ n_k $ el número de 
		individuos en el grupo $k$, $\bar{x}_{vk}$ la media de los datos que corresponden a ese grupo y
		$ \bar{x}_v $ la media total de los datos de la variable.
	\end{itemize}

	En este problema, las variables independientes son la latitud y la longitud.
	
	\subsection{Método de Elbow}
	El método de Elbow o método del codo representa una medida para cada número de grupos.
	Para ello, utiliza los valores del SSW, es decir, 
	la suma total de las distancias al cuadrado de los datos al centro de su grupo correspondiente. 
	Se obtiene un gráfico cuya forma se asemeja a la del siguiente:
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=11cm]{img/ejemplo_elbow}
		\caption{Ejemplo método de Elbow}
		\label{ejemplo_elbow}
	\end{figure}
	
	En la figura \ref{ejemplo_elbow} se puede comprobar que un aumento del número de grupos 
	supone una disminución en la curva. Sin embargo, el número de grupos 
	más interesante es aquel en el cual se ha producido la reducción más
	significativa. A este punto se le denomina codo. A partir de este punto, los aumentos 
	posteriores supondrán una reducción más continua y de menor interés.
	
	Este método puede que no proporcione información de 
	interés porque no exista un punto de inflexión tan claro y este método no 
	sea adecuado para el problema. Sin embargo, en el problema que aborda este TFG, este método proporciona 
	información relevante porque el número de grupos no pretende ser demasiado 
	alto y tienden a ser los adecuados. Además, hay que resaltar que el cliente buscar un número de grupos pequeños, 
	en el cuál se puede encontrar el punto de codo.
	
	Por otro lado, aunque existan métodos que proporcionen información acerca del número de grupos 
	más adecuado, puede ser que por exigencias del problema y del cliente se 
	desee un número determinado, aunque este no sea el más adecuado.
	
	\subsection{Dendrograma}
	En los métodos jerárquicos, la forma en la que se producen las uniones 
	se refleja en el dendrograma. En este contexto, un dendograma es un gráfico que ilustra 
	el proceso de agrupamiento. Para ello, muestra el proceso de unión y  la distancia a la 
	cual se realizan estas uniones. Cabe recordar que la distancia dependerá del método elegido.
	Estas uniones tienen forma de $ U $ inversa y el conjunto de ellas
	proporciona un diagrama en árbol. 
	
	En el eje $ X $ del gráfico se disponen los $ n $ elementos 
	iniciales, que se unen perpendicularmente al eje $ Y $ a un nivel de distancia determinado.
	
	Basándose en este dendrograma, se puede determinar el número de grupos adecuado. 
	Para ello, se determina una distancia máxima entre elementos de un mismo grupo. 
	Se traza una recta paralela al eje $ X $ que corte al eje $ Y $. Por debajo de dicha medida se obtiene 
	una clasificación de los grupos existentes a ese nivel y 
	los elementos que los forman.
	
	El dendrograma es útil cuando los puntos presentan una estructura jerárquica clara. 
	Este método debe realizarse cuidadosamente, ya que puede resultar engañoso cuando se aplica 
	ciegamente. Dos puntos pueden parecer próximos cuando no 
	lo están. Además, dos puntos pueden aparecer alejados cuando están próximos.
	
	La figura \ref{ejemplo_dendrograma} muestra un dendrograma, en el cual se ha tomado 
	una distancia de corte que da como resultado tres grupos. Estos grupos se encuentran inmediatamente debajo de 
	la línea de corte trazada. Estos son los
	grupos $ (ABC) $, $ (DE) $ y $ (FG) $.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=10cm]{img/ejemplo_dendrograma}
		\caption{Ejemplo dendrograma}
		\label{ejemplo_dendrograma}
	\end{figure}
	
	\section{Comparación de los métodos}
	Los métodos anteriormente detallados se usarán para dar solución al problema del cliente. 
	Además, es necesario seleccionar los algoritmos que mejor se adapten al problema. 
	Esta selección se realizará de forma muy similar a la 
	elección del número de grupos mediante el método de Elbow.
	
	La comparación de algoritmos se realiza mediante métricas de validación interna. 
	Dicha validación se realizará a partir de los datos agrupados.
	
	Las métricas de validación interna se basan en dos conceptos:
		
		\begin{itemize}
			\item Cohesión: los miembros de cada grupo deben estar lo más cerca 
			posible de los demás miembros del mismo grupo. Este concepto mide la menor 
			distancia intragrupal.
			
			\item Separación: los grupos están lo más alejados posible entre 
			ellos. Es decir, se crean grupos claramente diferenciados. Este concepto mide la
			mayor distancia intergrupal.
		\end{itemize}

	Estos dos conceptos son calculados, respectivamente, por el SSW y el SSB:
		
		\begin{itemize}
			\item SSW: un valor bajo representa mayor cohesión y un valor alto representa menor cohesión.

			\item SSB: un valor alto representa mayor separación y un valor bajo representa menor separación.

		\end{itemize}
	
	De modo que resulta interesante minimizar el primero y maximizar el segundo,
	lo cual suele ser equivalente, para obtener la mejor relación entre los dos 
	conceptos. Así, se pueden comparar los distintos métodos a través de sus 
	resultados. El ratio de comparación seleccionado en este TFG es el ratio de Ratkowsky-Lance, 
	el cual queda definido mediante la ecuación \ref{eqn:ratio} (\cite{2}).
				\begin{equation}\label{eqn:ratio}
				\dfrac{SSB}{SSB + SSW}
				\end{equation}

	En este ratio, la relación entre ambos conceptos se verá acentuada en 
	aquellos cuyo ratio sea mayor. De esta manera, se consideran soluciones de mayor calidad
	aquellas con ratios más elevados.
	