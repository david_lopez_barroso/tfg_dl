\chapter{Introducción}\label{Capitulo1}

\section{Presentación}
Hoy en día \textbf{la logística es fundamental para la gestión de recursos de las empresas} y se estima
que, en los principales sectores productivos de la economía, los costes logísticos totales suponen alrededor de un 6.5\% de su volumen de negocio (\cite{5}). 
Es por esto que se suelen crear áreas específicas dedicadas a ella. Estas ayudan a tomar buenas
decisiones estratégicas y minimizar costes. Una buena gestión logística permite mejorar la 
competitividad de la empresa.

La \textbf{importancia de las decisiones sobre la ubicación de instalaciones} se justifica por 
dos razones principales. Por un lado, suponen una inmovilización de recursos financieros 
a largo plazo debido a su costo. Además, una inversión mal efectuada puede ser muy difícil de recuperar
sin sufrir graves perjuicios económicos. Sin embargo, las compañías pueden optar por instalaciones
menos costosas o por arrendarlas. Por otro lado, son decisiones que afectan a la capacidad competitiva
de la empresa favoreciendo el desarrollo de su actividad en caso de tomarse correctamente.

Consecuentemente, la ubicación de almacenes tiene dos tipos de costes asociados. Por un lado,
aquellos costes derivados de la localización de los almacenes como pueden ser las decisiones de aperturas, cierres
y traslados.
Estas decisiones conllevan un coste elevado y afectan durante un intervalo de tiempo prolongado.
Por otro lado, aquellos fruto de la actividad normal son los costes de transporte. 

Este TFG se encuentra en el contexto de una beca en la empresa
Baobab Soluciones S.L., empresa spin-off de la ETSII
dedicada a ayudar a las empresas a tomar mejores decisiones 
haciendo uso de aplicaciones basadas en investigación operativa. A su vez, \textbf{el TFG forma parte de un proyecto de mayor envergadura}
realizado por Baobab para
uno de sus clientes. Este es \textbf{una importante empresa dedicada al sector eléctrico 
y gasístico}. Este proyecto se centra en las necesidades del cliente
para reducir los costes logísticos referidos a la localización de sus almacenes. 

A grandes rasgos, el proyecto sitúa y establece una planificación temporal de los almacenes del cliente 
de forma correcta. \textbf{El cliente, en su operación, acomete obras que son asignadas a diversas 
contratas}. Estas se encargan de las obras a realizar, \textbf{las cuales requieren de
material} a la medida que avanzan. Este material \textbf{debe ser suministrado a través 
de almacenes de abastecimiento}.

Los costes del sistema logístico se clasifican en dos tipos: \textbf{costes de transporte y costes de 
localización}. Los costes de transporte son el resultado de trasladar los materiales requeridos 
en una contrata desde el almacén que la abastece. Por su lado, los costes de localización son
aquellos referidos a aperturas, cierres o traslados de los almacenes.

En estos términos, \textbf{el objetivo del proyecto es el de proporcionar la localización de los almacenes para minimizar el coste, 
definido como la suma de los costes de localización y de transporte del sistema logístico durante
un horizonte temporal de estudio}.

El proyecto incluye el desarrollo de una herramienta que 
ayuda a la toma de decisiones sobre la planificación de la localización de almacenes. Además, 
para cumplir con las necesidades del cliente, \textbf{el proyecto incluye el desarrollo de dos modelos}: uno 
de prospección y otro de localización. Estos modelos son
independientes, pero interactúan entre sí y sus objetivos son los siguientes:
\begin{itemize}
	\item Modelo de prospección: permite \textbf{obtener las zonas de interés
	donde buscar posibles emplazamientos} para los almacenes.
	\item Modelo de localización: asigna los 
	almacenes necesarios. Además, planifica la gestión de la localización de los mismos con el 
	menor coste asociado posible.
\end{itemize}

El diagrama de la figura \ref{Diagrama_proyecto} sitúa el TFG en el marco del proyecto. Este consiste en \textbf{realizar un estudio
que permite seleccionar la técnica de agrupamiento más adecuada para el cliente}. Esta técnica tendrá en cuenta las características de las diferentes
zonas geográficas en las que opera el cliente, el cambio en la demanda y la presencia de datos anómalos. Por tanto, este TFG forma parte 
del modelo de prospección.

\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{img/esquema}
	\caption{Diagrama del proyecto}
	\label{Diagrama_proyecto}
\end{figure}

\section{Justificación del trabajo}
La empresa necesita realizar una selección de los almacenes candidatos.
\textbf{Antes de proceder a la localización de los almacenes, es necesario proporcionar al cliente
una visión previa de los posibles emplazamientos}. Estos emplazamientos
servirán como orientación y serán proporcionados por el modelo de prospección.

\textbf{El modelo de localización} se basa en un modelo
de programación lineal. Este modelo de optimización minimiza los costes 
de transporte y localización del sistema logístico. Para ello asigna contratas a almacenes y 
\textbf{evalúa donde abrir, cerrar o trasladar los almacenes en un horizonte determinado}. 

A su vez, requiere de múltiples datos para modelar de forma correcta la realidad. 
Uno de los datos más importantes, y que da sentido al estudio que se realiza en 
este TFG, es la situación geográfica de los posibles almacenes. El modelo 
de localización utilizará estas ubicaciones para resolver el problema. 
De este modo, \textbf{el modelo se centra en la planificación de la localización de almacenes, pero no 
en su ubicación inicial}. 

Por otra parte, el cliente necesita saber donde ubicar los almacenes. Para ello requiere 
de \textbf{una herramienta que le permita conocer zonas de interés en las cuales ubicarlos}. 

Consecuentemente, esta búsqueda de emplazamientos define la localización 
de los almacenes candidatos y todos los costes asociados. Todos estos elementos
serán clave en el modelo de localización para minimizar los costes logísticos.

Por esta razón se crea \textbf{el modelo de prospección}, que centra los objetivos 
del TFG. El modelo \textbf{obtiene las zonas de interés donde posteriormente realizar la 
búsqueda de los emplazamientos finales}. Estos emplazamientos determinan 
las ubicación de los almacenes. Además, se usan como dato de entrada para el modelo de 
localización.

Para realizar esta función, el modelo utiliza dos datos relativos a las contratas:
coordenadas geográficas y demanda. A partir de ellos se calculan las coordenadas 
de las zonas de interés donde el cliente puede realizar la búsqueda final de los 
emplazamientos.

El modelo de prospección \textbf{se basa en algoritmos de agrupamiento para cumplir con su propósito}. Este tipo de 
algoritmos agrupan las contratas de forma que las que pertenecen al mismo grupo sean similares.
Estos grupos resultantes definen las zonas de interés.

Por otra parte, el cliente realizará obras en diversos países con distinta geografía. Por ello, 
es necesario garantizar el correcto agrupamiento de las contratas en cada país. Para ello, \textbf{este TFG 
compara los algoritmos más reconocidos para conocer cuáles de ellos se adecúan más a 
cada país}.

Además, debido a la naturaleza de los datos que definen las contratas, estos pueden contener
datos anómalos como, por ejemplo, islas. Adicionalmente, el cliente maneja una gran cantidad 
de datos entre los cuales se pueden encontrar datos incorrectos. Por tanto, se debe encontrar 
un algoritmo robusto ante datos anómalos.

Por otra parte, las contratas pueden variar su demanda. Por tanto, se hace de vital importancia 
analizar la sensibilidad de los algoritmos ante cambios en la demanda. 
Para ello se establecen distintos escenarios que permitan un mayor 
conocimiento de los algoritmos en los que se basan las técnicas. 

\textbf{En conclusión, surge la necesidad de conocer qué algoritmos de agrupamiento son 
los más adecuados en cada caso. Esta necesidad da lugar a la realización 
del presente TFG.}

Una vez realizada la introducción del proyecto y de los modelos, así como justificado y motivado 
este TFG, se definen los objetivos del mismo en la siguiente sección.

\section{Objetivos}
\textbf{El objetivo general del TFG consiste en establecer qué algoritmo de agrupamiento 
es el más adecuado para satisfacer las necesidades del cliente}. Para lograr este propósito 
se definen una serie de objetivos específicos:

	\begin{itemize}
		\item	\textbf{Desarrollo de una herramienta en Python que implemente los algoritmos seleccionados}. 
		Además, esta herramienta facilita la obtención de datos necesarios para el estudio de los algoritmos.
		\item	\textbf{Estudio del comportamiento de las técnicas de agrupamiento} seleccionadas ante
		datos de distintos países. 
		\item   \textbf{Selección del algoritmo más adecuado en cada país}.
		\item	\textbf{Estudio de rendimiento y tiempo computacional}.
		\item	\textbf{Integración en el proyecto del estudio realizado en este TFG}.
	\end{itemize}


\section{Metodología}
La metodología seguida para la realización del TFG se puede dividir 
en las fases siguientes:

	\begin{itemize}
		\item Primera fase: \textbf{analizar las necesidades del cliente}. Determinar las características que deben cumplir los algoritmos de agrupamiento en el problema del cliente.
		\item Segunda fase: \textbf{seleccionar los algoritmos de agrupamiento más adecuados para el cliente}.
		Estos incluyen jerárquicos (enlace simple, enlace promedio, enlace completo, 
		método de Ward y BIRCH), no jerárquicos (k-medias) y basados en técnicas estadísticas (EM).
		\item Tercera fase: \textbf{implementación de los algoritmos} definidos en la segunda fase. Se 
		desarrolla una herramienta que permite obtener los datos necesarios para el estudio 
		de los algoritmos.
		\item Cuarta fase: \textbf{analizar los resultados obtenidos}. Se establece un criterio de comparación de algoritmos. 
		Con este criterio se determina el algoritmo más adecuado a cada país.
		\item Quinta fase: \textbf{realizar el análisis de sensibilidad} ante distintos escenarios de demanda. Se consideran tres escenarios: ácido, neutro y base. Partiendo de un escenario
		neutro con los datos originales, el escenario ácido presenta una disminución y el escenario 
		básico un aumento de la demanda. Se analiza el comportamiento de los algoritmos ante cada escenario y se busca el más estable.
		\item Sexta fase: \textbf{analizar la robustez de los algoritmos} ante la presencia de datos anómalos. Para ello, se analizan las 
		características de posibles datos anómalos y se discute el comportamiento de cada algoritmo ante ellos. 
		\item Séptima fase: recoger las \textbf{conclusiones} obtenidas de la realización del TFG.
	\end{itemize}

\section{Estructura del documento}
Este TFG consta de ocho capítulos y varios anexos estructurados como sigue:

En el capítulo \ref{Capitulo1} se realiza la \textbf{introducción del entorno, 
objetivos y metodología del trabajo}.

El capítulo \ref{Capitulo2} presenta el estado del arte y ocupa la 
\textbf{explicación de los distintos algoritmos de agrupamiento} estudiados. También
explica conceptos entre los que destacan: la elección del número de grupos y 
el criterio de comparación entre los distintos algoritmos.

El capítulo \ref{Capitulo3} trata las \textbf{características del problema y de los modelos
con más profundidad}. Para ello, se detalla las relaciones entre los elementos de la 
red logística y los parámetros necesarios para el modelado del problema.  

El capítulo \ref{Capitulo4} trata la \textbf{elección de Python como lenguaje de programación} 
del proyecto y del TFG. Tanto el modelo original como el estudio se han realizado 
en el mismo lenguaje de programación. Además, incluye \textbf{pseudocódigo de la 
lógica desarrollada}.

El capítulo \ref{Capitulo5} \textbf{expone y analiza los resultados de los diferentes métodos de agrupamiento}. En él se 
discute detenidamente qué método puede ser el más adecuado para un conjunto de datos. 
Para ello se analiza la sensibilidad de los algoritmos ante distintos escenarios de demanda. 
Además, se estudia la robustez de los mismos ante la presencia de outliers.

Los capítulos \ref{Capitulo6} y \ref{Capitulo7} presentan, respectivamente, tanto el \textbf{presupuesto} como la \textbf{planificación 
temporal} de acuerdo con la realización del trabajo.

El capítulo \ref{Capitulo8} finaliza el TFG recogiendo las \textbf{conclusiones} generales y obtenidas 
en el capítulo \ref{Capitulo5}. Además, explora nuevas \textbf{líneas futuras de desarrollo} 
y las diferentes \textbf{competencias desarrolladas} por el alumno mediante las 
actividades llevadas a cabo en la realización del trabajo.