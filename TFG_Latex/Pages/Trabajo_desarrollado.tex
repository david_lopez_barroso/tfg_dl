\chapter{Desarrollo del trabajo}\label{Capitulo4}
El tratamiento de los datos de entrada y la obtención de resultados gráficos
y numéricos que son objeto del estudio necesitan ser implementados mediante 
software informático. Este capítulo se centra en la elección de la 
herramienta y la descripción de los pasos seguidos para obtener los resultados. 

	\section{Elección de la herramienta}
	El estudio llevado a cabo precisa de una herramienta de software para proceder 
	a su implementación. En este caso, dicha herramienta se traduce en un
	lenguaje de programación y un entorno de desarrollo. El lenguaje de programación 
	elegido es Python y el entorno es PyCharm.
	
	Python es un lenguaje de programación interpretado y de código abierto. Su
	filosofía, altamente conocida, se caracteriza por un tipo de código que sigue 
	los principios de legibilidad y transparencia (\cite{13}). Principios que aúnan conceptos 
	como la simpleza, el orden, lo explícito o lo práctico.
	
	Las principales ventajas de este lenguaje de programación son: la
	facilidad de uso debido a la gestión automática de memoria y a operaciones sencillas de
	lectura y escritura, legibilidad del código (facilidad de lectura por su
	estructura), la abundancia de bibliotecas que extienden su funcionalidad y
	una gran base de usuarios que proporcionan disponibilidad de código y ayuda.
	
	Se debe mencionar que como Python es lenguaje
	interpretado necesita un intérprete. Lo cual puede representar una desventaja en 
	comparación con otros lenguajes más tradicionales que no necesitan intérprete. 
	Esto es debido a que los programas interpretados son más lentos que los compilados.
	Sin embargo, este tipo de programas suelen ser cortos y la diferencia inapreciable. 
	También, un problema extendido, pero que con el paso del tiempo se va atenuando, 
	es la falta de documentación para su uso.
	
	Sin embargo, los motivos que definen la elección de este lenguaje como
	elección para el desarrollo de este trabajo son las que corresponden al análisis
	de datos. Estos se pueden resumir en la existencia de numerosas librerías que
	implementan funciones para realizar cálculos matemáticos y estadísticos, que desarrollan
	algoritmos de aprendizaje automático (como los usados para este trabajo) y  que
	permiten la visualización y representación gráfica de datos.
	
	Estos motivos, junto al fácil y rápido aprendizaje, lo convierten en
	un lenguaje de calidad para el análisis de datos. 
	
	Existe otro motivo de gran importancia, aunque no estrictamente decisivo. El modelo de localización
	se implementa en Python. De esta manera, se unifica el lenguaje de programación para los dos modelos
	que componen el proyecto y para el estudio desarrollado en este TFG.
	
	En cuanto al entorno de desarrollo (IDE) usado, el elegido es PyCharm. Este es uno 
	de los entornos de desarrollo más populares y completos para Python. Entre
	sus ventajas se encuentra la existencia de un editor inteligente, el cual autocompleta
	código con atajos de teclado. Además, permite navegar por el código, haciendo el flujo
	de trabajo más dinámico. También, permite modificarlo sin
	comprometer la ejecución del mismo.
	
	\section{Sets de datos}
	Los sets de datos constituyen los datos de entrada. Es decir, los datos contienen la 
	información correspondiente a las contratas. Estos se utilizan para realizar el 
	estudio del que se encarga este TFG.
	
	Como ya se explicó en el apartado \ref{3.3.3}, los datos que
	caracterizan a las contratas son dos. Por un lado, las coordenadas geográficas que proporcionan 
	la ubicación de las contratas. Por otro lado, el histórico mensual de volumen de demanda por
	tipos de almacenaje.
	
	Los datos disponibles corresponden a dos países: España y Chile. Para cada uno de los cuales se pueden diferenciar 
	varios tipos de sets de datos. Estos se usan en diferentes fases del estudio.  
	
	\subsection{Sets de datos por geografía}\label{geografias}
	El cliente proporciona, para cada país, un número determinado de contratas con sus correspondientes 
	coordenadas. Sin embargo, cuando un set de datos es demasiado grande, este puede contener datos anómalos.
	Esto da lugar a varios tipos de sets determinados por las contratas que los componen. Además, también se pueden 
	introducir datos anómalos intencionadamente. De este modo, se puede realizar la clasificación siguiente:
		
		\begin{itemize}
			\item Datos originales: se componen por aquellos proporcionados por el cliente, los cuales pueden
			contener datos anómalos. Este tipo de datos se denominan outliers del cliente. Por ejemplo, contratas
			en el mar o en otro país anexo.
			\item Datos normalizados: contienen los datos correctos que se encuentran en los datos originales.
			\item Datos artificiales:  se componen por los datos normalizados y un número de datos anómalos
			introducidos de forma intencionada. Por ejemplo, contratas en islas o en zonas muy alejadas.
		\end{itemize}
	
	Cada tipo de datos, en cuanto a geografía se refiere, se utilizan para un propósito distinto. Por un 
	lado, los datos normalizados ayudan a conocer el número adecuado de grupos que deben introducirse en el 
	algoritmo. Además, los centros de estos grupos corresponden a las zonas más interesantes para la búsqueda
	de emplazamientos. Por otro lado, los datos originales y artificiales permiten realizar un análisis de robustez 
	de los algoritmos.
	
	De este modo, para cada país, se tienen los siguientes sets de datos:
		\begin{itemize}
			\item España
			\begin{itemize}
				\item Original: contratas peninsulares y varias en el mar Mediterráneo.
				\item Normalizado: contratas peninsulares.
				\item Artificial: contratas peninsulares y varias en las Islas Baleares y en las Islas Canarias.
			\end{itemize}
			\item Chile 
			\begin{itemize}
				\item Original: contratas en territorio chileno, varias en Bolivia, Argentina y en el océano 
				Pacífico.
				\item Normalizado: contratas en territorio chileno.
				\item Artificial: contratas en territorio chileno y varias en el extremo sur en la zona de 
				Magallanes.
			\end{itemize}
		\end{itemize}
	
	\subsection{Sets de datos por escenarios de demanda}\label{escenarios}
	En el caso de España se poseen las contratas y su histórico de demanda mensual durante los años 2015 y 2016. 
	Sin embargo, en el de Chile, sólo se poseen las contratas.
	
	A partir de los datos de la demanda de las contratas españolas, se crean diferentes escenarios de demanda.
    Se clasifican en tres:
	
		\begin{itemize}
			\item Neutro: corresponde a las demandas originales de las contratas por tipo de almacenaje.
			\item Ácido: representan una disminución en la demanda de dos veces la desviación típica de cada
			tipo de almacenaje.
			\item Básico: representan un aumento en la demanda de dos veces la desviación típica de cada tipo
			de almacenaje.
		\end{itemize}
	
	Esta serie de escenarios permite realizar un análisis de sensibilidad de los algoritmos. Para ello, se 
	comprueba su comportamiento ante un aumento o disminución de la demanda original.
	
	
	
	\section{Desarrollo}

	El desarrollo de los algoritmos anteriormente seleccionados y la obtención de los datos
	para estudiar su comportamiento se ha implementado en Python. 
	El código se estructura en una función principal y una serie de funciones específicas. La función 
	principal asigna los parámetros de inicio, realiza las lecturas y realiza llamadas al resto de subfunciones específicas. Las subfunciones han sido desarrolladas 
	para cumplir exclusivamente con los objetivos del TFG. A su vez,  
	hacen uso de funciones pertenecientes a librerías de libre acceso, las cuales serán citadas.

	 	\subsection{Función principal}
	 	La función principal define el flujo de la herramienta desarrollada. En esta función se
	 	introducen los parámetros que definen el caso de estudio. Seguidamente,
	 	se realizan las lecturas de los datos y las llamadas al resto de subfunciones. 
	 	
	 	Para inicializarla es necesario proporcionar el nombre del algoritmo, la ruta del set
	 	de datos y el número de grupos que se quieren estudiar. Este número de grupos
	 	puede ser un número concreto, $ k $, o un intervalo [$ k_0, k_n $] donde $ k_0 $ es el número inicial,
	 	$ k_n $ es el número final de grupos y $ k_0, k_1 \dots , k_n $ son números enteros.
	 	
		La función sigue la lógica recogida en el siguiente pseudocódigo:
	 		 	
	 	\fbox{
	 		\parbox{.8\textwidth}{
				\textbf{Función}
					Principal \\
					\textbf{Input} \\
					set de datos \\
					nombre algoritmo de agrupamiento \\
				 	número de grupos [$ k_0, k_n $] \\
				 	\textbf{Output} \\
				 	centroides \\
				 	mapa \\
				 	ratio \\
				 	gráfico de Elbow/dendrograma \\
				 	gráfico del ratio \\
				 	\textbf{Begin} \\
				 	lectura de los datos de entrada \\
				 	preparación de los datos de entrada \\
				 	\textbf{for} $ i $ in $range[k_0, k_n]$ \\
					\hspace*{1cm}asignación de grupos a cada contrata \\
					\hspace*{1cm}cálculo de los centroides de los grupos \\
					\hspace*{1cm}creación de un mapa \\
				 	\hspace*{1cm}cálculo del SSW \\
				 	\hspace*{1cm}cálculo del SSB \\
				 	\hspace*{1cm}cálculo del ratio \\
				 	gráfico del método de Elbow/dendrograma\\
				 	gráfico del ratio \\
				 	\textbf{End} \\
	 		}
	 	}
		
		Cada una de las acciones que se realizan tras la lectura de los datos de entrada se realizan
		mediante llamadas a las siguientes subfunciones.
	 	
	 	\subsection{Preparación de los datos}
	 	Antes de poder proceder al análisis del conjunto de datos que desea tratarse, se necesita
	 	una preparación de los mismos. Se recuerda que los datos de entrada son	las coordenadas geográficas 
	 	de las contratas, latitud y longitud, y la demanda.
	 	
	 	Por un lado, los costes de transporte dependen de la distancia entre el almacén y la contrata. Esta distancia
	 	se traducirá en costes de kilometraje. 
	 	Por otro lado, el volumen de demanda también es importante, ya que una contrata con gran volumen de demanda 
	 	requiere un mayor número de entregas de material que otra con menor demanda. Debido a esto, es interesante que las contratas
	 	con más demanda tengan mayor relevancia en el algoritmo y se encuentren más cercanas a los centros de los grupos.
	 	
	 	Para ello, se decide asociar a cada contrata, por cada zona de almacenaje, un peso en función de
	 	su demanda. Este peso dotará de mayor importancia en el algoritmo a contratas con mayor demanda 
	 	sobre las de menor demanda. Cada contrata se representa mediante uno o dos puntos en una mismas coordenadas
	 	geográficas. Es decir, existe un punto por cada zona de almacenaje (campa y cubierta).
	 	A continuación, se replican tantos puntos como indique el peso asociado sobre las coordenadas geográficas de la contrata. 
	 	De manera que,
	 	al existir más puntos en las mismas coordenadas, los algoritmos tienden a establecer las agrupaciones entorno a estas.
	 	
	 	En el caso del conjunto de datos pertenecientes a Chile, no se poseen datos
	 	de demanda. Por lo que pueden obviar esta preparación. Pudiendo realizar el agrupamiento posterior
	 	únicamente con las coordenadas geográficas de las contratas.
	 	
	 	Sin embargo, en el caso de España, se poseen tanto las coordenadas geográficas como 
	 	la demanda de los años 2015 y 2016, clasificada por meses y zona de almacenaje (campa y cubierta).
	 	Esto obliga a realizar un tratamiento para incluir estos valores en el algoritmo. 
	 
	 	Estos pesos se calculan a partir de la demanda umbral. Este parámetro se define como la demanda límite a partir
	 	de la cual se consideran elevadas. Su valor corresponde al del tercer cuartil de los datos. Esto significa que el 75\% de las demandas 
	 	se encuentra por debajo de este valor.
	 	
	 	Por ejemplo, para el escenario neutro de demanda en España, se obtiene el histograma de la figura
	 	\ref{histograma_neutro}.
	 	
	 	\begin{figure}[h]
	 		\centering
	 		\includegraphics[width=9cm]{img/histograma_neutro}
	 		\caption{Histograma de demandas del escenario neutro en España}
	 		\label{histograma_neutro}
	 	\end{figure}
 	
 		En él se observa cómo la mayoría de las demandas se encuentra
 		por debajo de 50.000. De este modo, se realizan replicaciones de aquellas zonas de almacenaje cuyas 
 		demandas superen este valor.
 		
 		Esto también puede observarse mediante un diagrama de cajas como el que se representa en
 		la figura \ref{boxplot_neutro}.
 		
 		\begin{figure}[H]
 			\centering
 			\includegraphics[width=9cm]{img/boxplot_neutro}
 			\caption{Diagrama de cajas del escenario neutro en España}
 			\label{boxplot_neutro}
 		\end{figure}
 	
 		En este diagrama se observa de nuevo el valor de 50.000 para el tercer cuartil. Además, 
	 	se identifican una serie de valores por encima del límite superior. Estas demandas 
	 	tan superiores supondrán un peso mayor para las correspondientes zonas de almacenaje
	 	de las contratas.
	 	
		Se realiza un estudio análogo para determinar la demanda umbral en los escenarios de demanda 
		ácido y básico.
	 	
	 	\begin{figure}[H]
	 		\begin{minipage}[b]{0.5\linewidth}
	 			\centering
	 			\includegraphics[width=\linewidth]{img/boxplot_acido}
	 			\caption{Diagrama de cajas del escenario ácido en España}
	 			\label{boxplot_acido}
	 		\end{minipage}
	 		\hspace{0.5cm}
	 		\begin{minipage}[b]{0.5\linewidth}
	 			\centering
	 			\includegraphics[width=\linewidth]{img/boxplot_basico}
	 			\caption{Diagrama de cajas del escenario básico en España}
	 			\label{boxplot_basico}
	 		\end{minipage}
	 	\end{figure}
	 	
	 	Los valores de demanda umbral para los distintos escenarios de demanda en España
	 se resumen en la tabla \ref{Tabla4.1}.
	 	
	 	\begin{table}[H]
	 		\begin{center}
	 			\begin{tabular}{|l|l|}
	 				\hline
	 				\rowcolor{colSubsec!85!}
	 				\textbf{\color{white}Escenario} & \textbf{\color{white}Demanda umbral} \\
	 				\hline \hline
	 				Ácido & 32.000 \\ \hline
	 				Neutro & 50.000 \\ \hline
	 				Básico & 180.000 \\ \hline
	 			\end{tabular}
	 			\caption{Tabla de demanda umbral}
	 			\label{Tabla4.1}
	 		\end{center}
	 	\end{table}
	 	
	 	Para calcular el peso de cada zona de almacenaje por contrata, se realiza el cociente
	 	entre la suma de la demanda de cada zona de almacenaje y el valor de la demanda umbral. 
	 	Este valor indica cuántas veces se replican los puntos correspondientes.
	 	
	 	El proceso que se lleva a cabo se concreta en los cuatro pasos siguientes: 
	 		\begin{enumerate}
	 			\item Filtrado y eliminación de las contratas que pueden encontrarse en el set de datos de demandas y no en el set de coordenadas de las contratas.
	 			\item Agrupación de las demandas sumando la demanda de todos los meses de estudio, por tipo de almacenaje y contrata. Se obtienen $a$ zonas de almacenaje diferentes.
	 			\item Cálculo del peso de cada zona de almacenaje de cada contrata.
	 			\item Replicación de las zonas de almacenaje de cada contrata. Para cada zona, se crean tantos puntos como indique el valor
	 			de su peso.
	 		\end{enumerate}
	 	
	 	La función sigue la lógica recogida en el siguiente pseudocódigo:
	 	
	 	\fbox{%
	 		\parbox{.8\textwidth}{
	 			\textbf{Función} Preparación de datos \\
	 			\textbf{Input} \\
	 			set de coordenadas de las contratas \\
	 			set de datos de demandas mensuales \\
	 			demanda umbral \\
	 			\textbf{Output} \\
	 			set de coordenadas de las zonas de almacenaje replicadas \\
	 			\textbf{Begin} \\
	 			filtrado de las contratas \\
	 			agrupación de las demandas \\
	 			asignación del peso \\
	 			\textbf{for} $ i $ in $range(0, a] $ \\
	 			\hspace*{1cm}$ n $	= $ pesoAlmacen_i $ \\
	 			\hspace*{1cm}\hspace*{1cm}\textbf{for} $ j $ in $range(0,  n)$ \\
	 			\hspace*{1cm}\hspace*{1cm}\hspace*{1cm}creación punto nuevo\\
	 			\textbf{End} \\
	 		}
	 	}
	 	
	 	
	 	\subsection{Asignación de grupos}
	 	Esta función es la más importante. En ella se realiza la asignación de cada zona de almacenaje
	 	de las contratas a los grupos. Estas zonas pueden haber sido replicadas, o no, en la función anterior.
	 	Este
	 	resultado determina el resto de valores que se calculan para realizar
	 	los análisis correspondientes de la calidad de la solución.
	 	
	 	La asignación proporciona como resultado un vector de una dimensión igual al total de las zonas de almacenaje de 
	 	las contratas y sus posibles replicaciones.
	 	Sus valores estarán en un intervalo definido entre cero y el número de grupos introducido menos uno. Cada uno
	 	de estos valores corresponde a cada uno de los diferentes grupos que se desean crear. De esta forma, las contratas
	 	que pertenecen a un mismo grupo son similares.
	 	
	 	La asignación de grupos se realiza mediante los algoritmos detallados en el capítulo \ref{Capitulo2}. Su implementación en Python 
	 	se realiza mediante las siguientes librerias gratuitas de Python: \textit{scipy} y \textit{sklearn}. A su vez, las funciones utilizadas
	 	para la implementación de los diferentes algoritmos son:
	 	
	 		\begin{itemize}
	 			\item Algoritmos jerárquicos: \textit{scipy.cluster.hierarchy}.
	 			\item K-medias: \textit{sklearn.cluster.kmeans}.
	 			\item BIRCH: \textit{sklearn.cluster.birch}.
	 			\item EM: \textit{sklearn.mixture.gaussianmixture}.
	 		\end{itemize}
	 	
	 	Además, haciendo uso de dichas funciones, se crean los grupos siguiendo los siguientes pasos:
	 	 	\begin{itemize}
			 	\item Inicialización y conversión de los datos de las zonas de almacenaje de las contratas al formato
			 	de datos con el que trabaja la función de cada algoritmo mediante la función $ .fit() $.
			 	\item Agrupación de las zonas de almacenaje de las contratas en diferentes grupos. 
			 		Se obtiene un vector con el número de grupo asignado a cada zona mediante la función $ .predict() $.
			 \end{itemize}		
	 	
	 	La función sigue la lógica recogida en el siguiente pseudocódigo:
	 	
	 	\fbox{
	 		\parbox{.8\textwidth}{
	 			\textbf{Función}
	 			Asignación de grupos \\
	 			\textbf{Input} \\
	 			nombre algoritmo de agrupamiento \\
	 			número de grupos \\
	 			set de coordenadas de zonas de almacenaje replicadas \\
	 			\textbf{Output} \\
	 			vector con los grupos asignados a zona de almacenaje \\
	 			\textbf{Begin} \\
	 			df = set de coordenadas \\
	 			df = df.fit() \\
	 			agrupacion = df.predict() \\
	 			\textbf{End} \\
	 		}
	 	}
 	
	 	\subsection{Cálculo de centros y creación de mapa}
	 	Una vez preparados los datos y realizadas las asignaciones, se obtienen los centroides de cada grupo creado.
	 	Los centroides de los grupos tienen gran valor, debido a que definen las zonas de interés que se proporcionarán al 
	 	cliente.
	 	
	 	Los centroides se componen de dos coordenadas: latitud y longitud. Para obtenerlos se realiza la media
	 	de las latitudes y las longitudes de los individuos de cada grupo.
	 	
		\begin{equation}
		centro_k = (\frac{\sum_j{latitud_j}}{n_k}, \frac{\sum_j{longitud_j}}{n_k})
		\end{equation}
		donde $ centro_k $ es el centroide del grupo $ k $, $ latitud_j $ y $ longitud_j $ 
		son las coordenadas geográficas de los individuos del grupo $ k $ y $ n_k $ el número 
		de individuos del grupo $ k $.
	 	
	 	En este paso se puede observar
	 	la importancia de los pesos y la replicación de datos realizada con anterioridad. Además de influir directamente
	 	en los algoritmos al realizar las asignaciones, la existencia de puntos replicados en un mismo lugar influye
	 	en el cálculo de los centroides. Los cuales estarán más próximos a los puntos replicados que al resto. Como consecuencia, las contratas
	 	con mayor demanda se encontrarán más cerca de los centroides, en comparación con otras contratas que presentan menos demanda.
	 	
	 	En este momento se conocen las asignaciones y los centros de cada grupo. Por lo que se pueden imprimir estos 
	 	resultados en un mapa que ilustre la solución. Esto se hace con la librería de código abierto $ plotly $, la cual permite representar 
	 	las contratas y los centroides en un mapa. La figura \ref{4.5} muestra un grupo creado en Galicia. Las contratas se representan por puntos verdes 
	 	y el centroide por un punto rojo.
	 	Sin embargo, antes habrá que eliminar las replicaciones, si existen, para mostrar la situación de cada contrata como un
	 	único punto.
	 	
	 	\begin{figure}[H]
	 		\centering
	 		\includegraphics[width=8cm]{img/ejemplo_mapa}
	 		\caption{Ejemplo de mapa realizado con plotly}
	 		\label{4.5}
	 	\end{figure}
	 	
	 	Estos pasos responden al siguiente psudocódigo:
	 	
	 	\fbox{
	 		\parbox{.8\textwidth}{
	 			\textbf{Función} 
	 			Cálculo de los centros y creación de mapa \\
	 			\textbf{Input} \\
	 			set de coordenadas de almacenes replicados \\
	 			vector con los grupos asignados a cada almacén \\
	 			\textbf{Output} \\
	 			centros de los grupos creados \\
	 			mapa con los centroides y las contratas \\
	 			\textbf{Begin} \\
	 			\textbf{if} $ duplicates $ \\
	 			\hspace*{1cm} eliminación de replicaciones \\
				cálculo de los centroides de cada grupo \\
	 			creación del mapa con los centroides y las contratas \\
	 			\textbf{End} \\
	 		}
	 	}
	 	
	 	\subsection{Cálculo de SSW}
	 	Esta función calcula el SSW de la agrupación.
	 	Cabe recordar que el SSW representa la cohesión entre los miembros de un mismo grupo. Esto significa que
	 	a menor SSW, más similares son entre sí los datos del grupo. Es decir, menor distancia intergrupal. 
	 	Su cálculo es expresado por la ecuación \ref{eqn:within}. 
	 	
	 	Sin embargo, la presente
	 	función calcula el SSW de cada variable por separado. Por ello,
	 	se procede calculando el SSW de la latitud por un lado y de la longitud por otro. El SSW de la agrupación corresponde
	 	a la suma del SSW de ambas variables independientes mediante dos llamadas a la función.
	 	
	 	La función sigue la lógica recogida en el siguiente pseudocódigo:
	 	
	 	\fbox{
	 		\parbox{.8\textwidth}{
	 			\textbf{Función} \\
	 			Cálculo de SSW \\
	 			\textbf{Input} \\
	 			set de datos de la variable \\
	 			vector con los grupos asignados a cada zona de almacenaje \\
	 			\textbf{Output} \\
	 			valor del SSW \\
	 			\textbf{Begin} \\
	 			\textbf{for} $ i $ in $range[k_0, k_n]$ \\
	 			\hspace*{1cm}$ data $ = datos pertenecientes al grupo $ k $\\
	 			\hspace*{1cm}cálculo del SSW del grupo $ k $ \\
	 			suma de SSW de todos los grupos \\
	 			\textbf{End} \\
	 		}
	 	}
	 	
	 	\subsection{Cálculo de SSB}
	 	El cálculo de SSB se realiza de forma análoga al de SSW. En este caso, SSB representa
	 	la separación entre los distintos grupos que componen la asignación. Esto significa que a mayor between, más
	 	distanciados están entre sí los grupos. Es decir, mayor distancia intergrupal. Su cálculo es expresado por la ecuación 
	 	\ref{eqn:between}. 
	 	
	 	Sin embargo, la presente función calcula el SSB de cada variable por separado. Por ello,
		se procede calculando el SSB de la latitud por un lado y de la longitud por otro. El SSB de la agrupación corresponde
		a la suma del SSB de ambas variables independientes mediante dos llamadas a la función.
	 	
	 	La función sigue la lógica recogida en el siguiente pseudocódigo:
	 		 	
	 	\fbox{
	 		\parbox{.8\textwidth}{
	 			\textbf{Función} 
	 			Cálculo de SSB \\
	 			\textbf{Input} \\
	 			set de datos de la variable \\
	 			vector con los grupos asignados a cada zona de almacenaje \\
	 			\textbf{Output} \\
	 			valor del SSB \\
	 			\textbf{Begin} \\
	 			$ mediaTotal $ = media de la variable \\
	 			\textbf{for} $ i $ in $range[k_0, k_n]$ \\
	 			\hspace*{1cm} $ data $ = datos pertenecientes al grupo $ k $\\
	 			\hspace*{1cm}cálculo de SSB del grupo $ k $ \\
	 			suma de SSB de todos los grupos \\
	 			\textbf{End} \\
	 		}
	 	}
 		
	    \subsection{Creación de gráficos}
	    
	    	\subsubsection{Gráfico de Elbow}\label{Elbow}
	    Como se ha explicado anteriormente, el SSW se utiliza para representar el método de Elbow. Este es un gráfico
	    que representa el valor de SSW de asignaciones realizadas con números de grupos diferentes. 
	    
	    A su vez, este gráfico ayuda a la elección del número de grupos de mayor rendimiento. Siendo este el correspondiente a una 
	    mayor reducción del valor de SSW. Sin embargo, son de interés todos aquellos que proporcionan una reducción significativa,
	    hasta el punto en el que esta es muy baja o prácticamente nula.
	    
	    Además, debe tenerse en cuenta las necesidades del cliente, ya que el número de grupos tiene que ser realista
	    para poder cumplir con su propósito.

    	En la figura \ref{ejemplo_elbow2} se observa cómo, al aumentar el número de grupos, el SSW es menor. Esto se debe a que al
		existir más grupos, sus respectivas contratas estarán más próximas al centroide de cada grupo.	    
		
	    \begin{figure}[H]
	    	\centering
	    	\includegraphics[width=9cm]{img/ejemplo_elbow2}
	    	\caption{Ejemplo del método de Elbow con herramienta}
	    	\label{ejemplo_elbow2}
	    \end{figure}
    
    	En este ejemplo, el número adecuado de grupos es tres. Esto es porque entre los números de grupos dos y tres se produce una mayor
    	reducción del valor de SSW.
	    
	    	\subsubsection{Dendrograma}
	    En el caso de los algoritmos jerárquicos, la elección del número de grupos también se puede decidir a través del
	    dendrograma. De manera similar al método de Elbow, la elección del número de grupos adecuado corresponde a aquel
	    en el que la distancia a la que se unen los grupos sufre una mayor reducción. 
	    
	    En la figura \ref{ejemplo_dendrograma2} se representa el dendrograma para la misma muestra usada en el ejemplo anterior. 
	    Por ello, el número de grupos más adecuado es el mismo.
	    En dicho dendrograma se observa cómo, de nuevo, el número adecuado de grupos es tres. Esto se debe a
	    una reducción mayor de la distancia a la que se unen los grupos dos y tres.
	    
	    \begin{figure}[H]
	    	\centering
	    	\includegraphics[width=8.5cm]{img/ejemplo_dendrograma2}
	    	\caption{Ejemplo de dendrograma con herramienta}
	    	\label{ejemplo_dendrograma2}
	    \end{figure}
	    
	    \subsubsection{Ratio}\label{ratio}
	    
	    Una vez obtenidos el SSW y el SSB de la asignación realizada, se calcula el ratio
	    entre ellos. Este ratio responde a la ecuación \ref{eqn:ratio} y representa la relación entre los conceptos 
	    de distancia intragrupal e intergrupal. De este modo, un ratio mayor significa que las contratas de los grupos
	    estarán más cerca entre sí y que los diferentes grupos estarán más alejados. 
	    
		En definitiva, como es de interés que las contratas estén lo más cerca posible de los almacenes,
		un ratio mayor proporcionará una mejor solución de cara a estos intereses.

		Este ratio se usa para determinar la calidad de los algoritmos y compararlos en las diferentes situaciones posibles.
		Se muestra un ejemplo en la figura \ref{ejemplo_ratio}.	
		
	    \begin{figure}[H]
			\centering
			\includegraphics[width=9cm]{img/ejemplo_ratio}
			\caption{Ejemplo de representación del ratio con herramienta}
			\label{ejemplo_ratio}
		\end{figure}
	


	

	 	
	 	