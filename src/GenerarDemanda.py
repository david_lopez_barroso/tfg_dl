import numpy as np
import pandas as pd
import xlsxwriter
import copy as cp

NombreExcelEntrada = 'data/Esp_demandas_neutro.xlsx'
NombreHoja = 'demandas'
VarDispersionCampa = -0.5
VarDispersionCubierto = -0.5
PorcentajeClientes = 1
NombreExcelSalida = 'data/Esp_demandas_acido05.xlsx'

df = pd.read_excel(NombreExcelEntrada, sheet_name=NombreHoja)

Campa = cp.deepcopy(df[df.codigo_material == 'CAMPA'])
Cubierto = cp.deepcopy(df[df.codigo_material == 'CUBIERTO'])

DesviacionCampa = np.std(Campa['demanda'])
DesviacionCubierta = np.std(Cubierto['demanda'])

index = []
Dimension = len(Campa['codigo_contrata'])
stop = round(PorcentajeClientes * Dimension / 2, 0)

while len(index) != stop:
    index.append(int(round(np.random.uniform(0, Dimension), 0)))
    index = list(set(index))

for i in range(0, len(index)):
    Campa['demanda'].iloc[index[i]] = cp.deepcopy(
        Campa['demanda'].iloc[index[i]] + VarDispersionCampa * DesviacionCampa)

index = []
Dimension = len(Cubierto['codigo_contrata'])
stop = round(PorcentajeClientes * Dimension / 2, 0)
while len(index) != stop:
    index.append(int(round(np.random.uniform(0, Dimension), 0)))
    index = list(set(index))

for i in range(0, len(index)):
    Cubierto['demanda'].iloc[index[i]] = cp.deepcopy(
        Cubierto['demanda'].iloc[index[i]] + VarDispersionCubierto * DesviacionCubierta)

NuevaDemanda = pd.concat([Campa, Cubierto])
NuevaDemanda = pd.DataFrame(NuevaDemanda, columns=list(Campa.columns))

# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(NombreExcelSalida, engine='xlsxwriter', date_format='dd/mm/yyyy',
                        datetime_format='dd/mm/yyyy')

# Convert the dataframe to an XlsxWriter Excel object.
NuevaDemanda.to_excel(writer, sheet_name=NombreHoja, index=False)
writer.save()

