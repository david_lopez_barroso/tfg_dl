import sklearn.cluster as cl
import scipy.cluster.hierarchy as sci
from  sklearn.cluster import Birch
from sklearn.mixture import GaussianMixture
import plotly.tools as tls
import plotly.graph_objs as go
import plotly.offline as offline
import matplotlib.pyplot as plt
import numpy as np

def prepare_data(df_demanda, df_contratas, max_demand, duplicate):
    """
    Prepare the data to be used for clustering:
    - Aggregate demand for every "contrata"
    - Create more points to give more weight to "contratas" with high demand

    :param df_demanda: a dataframe with the demand of the "contratas"
    :param df_contratas: a dataframe with the coordinates of the "contratas"
    :param max_demand: the quantity of demand to create another point for the "contrata"
    :param duplicate: if true activate the creation of new points to represent high demand

    :return: A dataframe with the data to be used with clusters
    """

    # Aggregate demand
    df_demanda = df_demanda.groupby(by = ["codigo_contrata", "codigo_material"]).agg({"demanda":sum})

    df_demanda.to_excel('data/hola.xlsx')

    # If duplicate, assign the number of points to create
    if duplicate == True:
        df_demanda = df_demanda.assign(weight = np.ceil(df_demanda.demanda/max_demand))
    else:
        df_demanda = df_demanda.assign(weight=1)

    # Reset the index after the groupby
    df_demanda.reset_index(inplace=True)

    # Merge the locations and the demands
    df_merged = df_contratas.merge(df_demanda, on ='codigo_contrata', how ="inner")

    # Create the dataframe to return with several points for every weight
    df_data = df_merged[:]
    for i in range(0,len(df_merged)):
        n = int(df_merged["weight"][i])
        for j in range(1,n):
            df_data = df_data.append(df_merged.iloc[i,:])

    df_data.reset_index(inplace=True)

    return(df_data)

def create_clusters(df, n_clusters, func):
    """
    Function to create clusters

    :param df: a dataframe with two columns longitud and latitud in that order
    :param n_clusters: number of clusters to be create
    :param func: clustering method to use

    :return: an array with the cluster to assign to every point
    """

    # Create a list with the dataframe
    l_points = df.values.tolist()

    # Kmeans method
    if func == 'kmeans':
        # Create object
        kmeans = cl.KMeans(n_clusters)
        # Fit kmeans object to data
        kmeans.fit(l_points)
        # Create clusters
        a_clusters = kmeans.fit_predict(l_points)

    # Hierarchical method
    elif func == 'single' or func == 'complete' or func == 'average' or func =='ward':
        H_cluster = sci.linkage(l_points, func)
        # Create clusters
        a_clusters = sci.fcluster(H_cluster, n_clusters, criterion='maxclust')

    # Birch method
    elif func == 'birch':
        # Create object
        birch = Birch(branching_factor=50, n_clusters=n_clusters, threshold=0.5, compute_labels=True)
        # Fit birch object to data
        birch.fit(l_points)
        # Create clusters
        a_clusters = birch.fit_predict(l_points)

    # EM method
    elif func == 'EM':
        # Create object
        em = GaussianMixture(n_components = n_clusters)
        # Fit EM object to data
        em.fit(l_points)
        # Create clusters
        a_clusters = em.predict(l_points)

    return a_clusters

def show_map(clusters, df, data_name, func, duplicate):
    """
    Generate centres and create a map of the clusters

    :param clusters: vector with clusters assigned to the points
    :param df: a dataframe with the coordinates to be used with clusters
    :param data_name: string with text to be used in figures
    :param func: used clustering method
    :param duplicate: indicates if data contains duplicates

    :return: coordinates of the centers
    """

    s_clust = set(clusters)

    # Select the clusters without the duplicate values and prepare some info parameters for the map
    if duplicate == True:
        result_length = len(df.groupby(by=["codigo_contrata", "codigo_material"]))
        df_data_result = df.iloc[range(0, result_length)]
        map_center_lat = 40
        map_center_long = -4
    elif duplicate == False:
        df_data_result = df
        map_center_lat = -33.47269
        map_center_long = -70.64724

    longs = df['longitud']
    lats = df['latitud']
    centers_long = []
    centers_lat = []
    data = []

    # Credentials
    mapbox_access_token = 'pk.eyJ1IjoiZGxvcGJhciIsImEiOiJjanFzaHV6ZGIwbXU5NDhsam1uYjJpYzI2In0.bBsHqHzzPLBzv534sJGUig'
    tls.set_credentials_file(username='dlopbar', api_key='V66CNPo0AKWIPfOP82LK')

    # Create centers and prepare data for the map
    for c in s_clust:
        # Prepare data to plot
        l_in_cluster = [i for i in range(0, len(clusters)) if clusters[i] == c]
        # Calculate centers
        long_center = np.nanmean(longs[l_in_cluster])
        lat_center = np.nanmean(lats[l_in_cluster])
        centers_long.append(long_center)
        centers_lat.append(lat_center)
        # Draw the cluster and the center
        l_in_cluster = [i for i in range(0, len(clusters)) if clusters[i] == c]
        data += [go.Scattermapbox(lat=df_data_result["latitud"][l_in_cluster], lon=df_data_result["longitud"][l_in_cluster],mode='markers',marker=dict(size=8), name='Cluster ' + str(c))] + [go.Scattermapbox(lat=[lat_center], lon=[long_center],mode='markers',marker=dict(size=20, color='red'), name='Centro ' + str(c), showlegend=False)]
        # data += [go.Scattermapbox(lat=df_data_result["latitud"][l_in_cluster], lon=df_data_result["longitud"][l_in_cluster],mode='markers', marker=dict(size=8), name='Cluster ' + str(c))]

    # Create figure
    layout = go.Layout(autosize=True,hovermode='closest', title=str(func) + ' ' + str(data_name),mapbox=dict(accesstoken=mapbox_access_token, bearing=0,center=dict(lat=map_center_lat, lon=map_center_long),pitch=0,zoom=5),)
    fig = dict(data=data, layout=layout)
    # Show figure
    offline.plot(fig)

    return(centers_long, centers_lat)

def calcWithinGroupsVariance(variable, groupvariable):
    """
    Calculate total sum of within groups variance

    :param variable: an array with the lats or longs of the points
    :param groupvariable: an array with clusters assigned to the points

    :return: total sum of within groups variance of current variable
    """

    # Find out how many values the group variable can take
    levels = sorted(set(groupvariable))
    numlevels = len(levels)
    # Get the mean and standard deviation for each group:
    numtotal = 0
    denomtotal = 0
    for leveli in levels:
        levelidata = variable[groupvariable==leveli]
        levelilength = len(levelidata)
        # Get the standard deviation for group i:
        sdi = np.std(levelidata)
        numi = (levelilength)*sdi**2
        denomi = levelilength
        numtotal = numtotal + numi
        denomtotal = denomtotal + denomi
    # Calculate the within-groups variance
    Vw = numtotal # / (denomtotal - numlevels)
    return Vw

def calcBetweenGroupsVariance(variable, groupvariable):
    """
    Calculate total sum of between groups variance

    :param variable: an array with the lats or longs of the points
    :param groupvariable: an array with clusters assigned to the points

    :return: total sum of between groups variance of current variable
    """

    # Find out how many values the group variable can take
    levels = sorted(set((groupvariable)))
    numlevels = len(levels)
    # Calculate the overall grand mean:
    grandmean = np.mean(variable)
    # Get the mean and standard deviation for each group:
    numtotal = 0
    denomtotal = 0
    for leveli in levels:
        levelidata = variable[groupvariable==leveli]
        levelilength = len(levelidata)
        # get the mean and standard deviation for group i:
        meani = np.mean(levelidata)
        sdi = np.std(levelidata)
        numi = levelilength * ((meani - grandmean)**2)
        denomi = levelilength
        numtotal = numtotal + numi
        denomtotal = denomtotal + denomi
    # calculate the between-groups variance
    Vb = numtotal # / (numlevels - 1)
    return(Vb)

def show_elbowcode(total_within, n_cluster_inicial, num_clusters_final, data_name, func):
    """
    Plot values of WSS for different numbers of clusters (Elbow code)

    :param total_within: an array with the values of WSS for each number of clusters
    :param n_cluster_inicial: initial number of clusters
    :param num_clusters_final: final number of clusters
    :param data_name: string with text to be used in figures
    :param func: used clustering method

    :return: nothing
    """

    # Clear the plot
    plt.clf()
    # Plot data
    plt.plot(range(n_cluster_inicial, num_clusters_final + 1, 1), total_within,'bx-', linewidth=0.5, markersize=7)
    plt.title(str(func) + ' ' + str(data_name))
    plt.suptitle('Método de Elbow')
    plt.xlabel('Número de grupos')
    plt.ylabel('SSW')
    # Show plot
    plt.show()

def show_ratio(total_ratio, n_cluster_inicial, num_clusters_final, data_name, func):
    """
    Plot values of ratio (BSS/(BSS+WSS) for different numbers of clusters

    :param total_ratio: an array with the values of ratio (BSS/(BSS+WSS) for each number of clusters
    :param n_cluster_inicial: initial number of clusters
    :param num_clusters_final: final number of clusters
    :param data_name: string with text to be used in figures
    :param func: used clustering method

    :return: nothing
    """

    # Clear the plot
    plt.clf()
    # Plot data
    plt.bar(range(n_cluster_inicial, num_clusters_final + 1, 1), total_ratio, width = 0.5)
    plt.title(str(func) + ' ' + str(data_name))
    plt.xlabel('Número de grupos')
    plt.ylabel('Ratio SSB/(SSB + SSW)')
    # Show plot
    plt.show()